﻿using System;
using ParkingChargeCalculator.Factories;
using ParkingChargeCalculator.Models;

namespace MyParkApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("My Parking Charge App");

            // 1. A stay entirely outside of a chargeable period will return £0.00.
            var startTime = new DateTime(2017, 09, 07, 20, 50, 0);
            var endTime = new DateTime(2017, 09, 08, 5, 25, 0);

            IChargeDuration chargeDuration = new ShortStayChargeDuration(startTime, endTime);

            var chargeCalculator = ChargeCalculatorFactory.GetChargeCalculator(chargeDuration.Rate);
            var chargeToPay = chargeCalculator.CalculateCharge(chargeDuration);

            Console.WriteLine("1. rate: {0}, start: {1}, end {2}, duration = {4}, to pay = {3:C}", chargeDuration.Rate, chargeDuration.Start, chargeDuration.End, chargeToPay, chargeDuration.Duration());


            // 2. A short stay from 07/09/2017 16:50:00 to 09/09/2017 19:15:00 would cost £12.28.
            startTime = new DateTime(2017, 09, 07, 16, 50, 0);
            endTime = new DateTime(2017, 09, 09, 19, 15, 0);

            chargeDuration = new ShortStayChargeDuration(startTime, endTime);

            chargeCalculator = ChargeCalculatorFactory.GetChargeCalculator(chargeDuration.Rate);
            chargeToPay = chargeCalculator.CalculateCharge(chargeDuration);

            Console.WriteLine("2. rate: {0}, start: {1}, end {2}, duration = {4}, to pay = {3:C}", chargeDuration.Rate, chargeDuration.Start, chargeDuration.End, chargeToPay, chargeDuration.Duration());


            // 3. A long stay from 07/09/2017 07:50:00 to 09/09/2017 05:20:00 would cost £22.50.
            startTime = new DateTime(2017, 09, 07, 7, 50, 0);
            endTime = new DateTime(2017, 09, 09, 5, 20, 0);

            chargeDuration = new LongStayChargeDuration(startTime, endTime);

            chargeCalculator = ChargeCalculatorFactory.GetChargeCalculator(chargeDuration.Rate);
            chargeToPay = chargeCalculator.CalculateCharge(chargeDuration);

            Console.WriteLine("3. rate: {0}, start: {1}, end {2}, duration = {4}, to pay = {3:C}", chargeDuration.Rate, chargeDuration.Start, chargeDuration.End, chargeToPay, chargeDuration.Duration());


            Console.Write("Press any key to exit...");
            Console.ReadKey();

        }
    }
}
