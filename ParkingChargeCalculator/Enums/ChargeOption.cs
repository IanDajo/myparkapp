﻿namespace ParkingChargeCalculator.Enums
{
    /// <summary>
    /// Charge options used for calculation.
    /// </summary>
    /// <example>
    /// 1. A stay entirely outside of a chargeable period will return £0.00.
    /// 2. A short stay from 07/09/2017 16:50:00 to 0909/2017 19:15:00 would cost £12.28.
    /// 3. A long stay from 07/09/2017 07:50:00 to 09/09/2017 05:20:00 would cost £22.50.
    /// </example>
    public enum ChargeOption
    {
        /// <summary>
        /// £1.10 per hour between 8am ad 6pm on weekdays, free outside these times. Visits need not be whole hours and can last more than one day.
        /// </summary>
        ShortStay = 0,
        /// <summary>
        /// £7.50 per day or part day including weekends, so the minimum charge will be for one day.
        /// </summary>
        LongStay = 1
    }
}
