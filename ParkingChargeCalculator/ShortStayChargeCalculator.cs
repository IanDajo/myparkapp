﻿using ParkingChargeCalculator.Models;
using System;
using System.Linq;

namespace ParkingChargeCalculator
{
    public class ShortStayChargeCalculator : ChargeCalculator
    {
        private const double ChargeableRate = 1.10;
        private const int ChargeableStartHour = 8;
        private const int ChargeableStartMinute = 0;
        private const int ChargeableEndHour = 18;
        private const int ChargeableEndMinute = 0;
        private DayOfWeek[] ChargeChageableDaysOfWeek = { DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday };

        public override double CalculateCharge(IChargeDuration chargeDuration)
        {
            double amountToCharge = 0;
            double minutesToCharge = 0;

            //loop over each day
            for (var day = chargeDuration.Start.Date; day <= chargeDuration.End; day = day.AddDays(1))
            {
                var startHour = ChargeableStartHour;
                var startMinute = ChargeableStartMinute;
                var endHour = ChargeableEndHour;
                var endMinute = ChargeableEndMinute;

                //is it a chargeable weekday?
                if (ChargeChageableDaysOfWeek.Contains(day.DayOfWeek))
                {

                    //is it the first day of parking? Then we may need to adjust the start hour
                    if (chargeDuration.Start.Date == day)
                    {
                        //check if parked before chargeable start time
                        if (chargeDuration.Start.Hour > ChargeableStartHour)
                        {
                            startHour = chargeDuration.Start.Hour;
                            startMinute = chargeDuration.Start.Minute;
                        }
                    }

                    //is it the last day of parking? then we may need to adjust the end hour
                    if (chargeDuration.End.Date == day)
                    {
                        //check if parked before chargeable start time
                        if (chargeDuration.End.Hour < ChargeableEndHour)
                        {
                            endHour = chargeDuration.End.Hour;
                            endMinute = chargeDuration.End.Hour;
                        }
                    }

                    //how many chargeable minutes per day?
                    var todaysStartDateForCharge = new DateTime(1900, 1, 1, startHour, startMinute, 0);
                    var todaysEndDateForCharge = new DateTime(1900, 1, 1, endHour, endMinute, 0);
                    var chargePeriod = new TimeSpan(todaysEndDateForCharge.Ticks - todaysStartDateForCharge.Ticks);

                    minutesToCharge += chargePeriod.TotalMinutes;

                }

            }

            if (minutesToCharge > 0)
            {
                amountToCharge = Math.Round(minutesToCharge * (ChargeableRate / 60), 2);
            }

            return amountToCharge;

        }
    }
}
