﻿using ParkingChargeCalculator.Enums;

namespace ParkingChargeCalculator.Factories
{
    public static class ChargeCalculatorFactory
    {
        public static ChargeCalculator GetChargeCalculator(ChargeOption chargeOption)
        {
            switch (chargeOption)
            {
                case ChargeOption.LongStay:
                    return new LongStayChargeCalculator();
                default:
                    return new ShortStayChargeCalculator();
            }

        }

    }
}
