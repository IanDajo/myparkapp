﻿using ParkingChargeCalculator.Models;

namespace ParkingChargeCalculator
{
    public abstract class ChargeCalculator : IChargeCalculator
    {
        public ChargeCalculator() { }

        public abstract double CalculateCharge(IChargeDuration chargeDuration);

    }
}
