﻿using ParkingChargeCalculator.Models;
using System;

namespace ParkingChargeCalculator
{
    public class LongStayChargeCalculator : ChargeCalculator
    {
        private const double ChargeableRate = 7.50;

        public override double CalculateCharge(IChargeDuration chargeDuration)
        {

            // add 1 for minimum charge
            var days = Math.Ceiling(chargeDuration.SpanDuration().TotalDays) + 1;

            // charge = number of days * daily fee
            double charge = Math.Round(days * ChargeableRate, 2);

            return charge;

        }

    }
}
