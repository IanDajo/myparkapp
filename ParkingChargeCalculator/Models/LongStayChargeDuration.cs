﻿using ParkingChargeCalculator.Enums;
using System;

namespace ParkingChargeCalculator.Models
{
    public class LongStayChargeDuration : ChargeDuration
    {
        public LongStayChargeDuration(DateTime start, DateTime end) : base(start, end)
        {
            base.Rate = ChargeOption.LongStay;
        }
    }
}
