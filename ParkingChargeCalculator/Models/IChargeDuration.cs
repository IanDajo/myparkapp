﻿using ParkingChargeCalculator.Enums;
using System;

namespace ParkingChargeCalculator.Models
{
    public interface IChargeDuration
    {
        ChargeOption Rate { get; set; }

        DateTime Start { get; }

        DateTime End { get; }

        string Duration();

        TimeSpan SpanDuration();
    }
}