﻿using ParkingChargeCalculator.Enums;
using System;

namespace ParkingChargeCalculator.Models
{
    public abstract class ChargeDuration : IChargeDuration
    {
        public ChargeOption Rate { get; set; }
        public DateTime Start { get; }
        public DateTime End { get; }

        public ChargeDuration(ChargeOption rate, DateTime start, DateTime end) : this(start, end)
        {
            Rate = rate;
        }

        public ChargeDuration(DateTime start, DateTime end)
        {
            Start = start;
            End = end;
        }

        public TimeSpan SpanDuration()
        {
            return new TimeSpan(End.Ticks - Start.Ticks);
        }

        public string Duration()
        {
            string duration = string.Empty;

            var spanDuration = new TimeSpan(End.Ticks - Start.Ticks);

            duration = spanDuration.ToString("c");

            return duration;

        }

    }
}
