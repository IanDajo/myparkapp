﻿using ParkingChargeCalculator.Enums;
using System;

namespace ParkingChargeCalculator.Models
{
    public class ShortStayChargeDuration : ChargeDuration
    {
        public ShortStayChargeDuration(DateTime start, DateTime end) : base(start, end)
        {
            base.Rate = ChargeOption.ShortStay;
        }
    }
}
