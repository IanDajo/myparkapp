﻿using ParkingChargeCalculator.Models;

namespace ParkingChargeCalculator
{
    public interface IChargeCalculator
    {
        double CalculateCharge(IChargeDuration chargeDuration);
    }
}