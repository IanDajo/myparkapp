using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParkingChargeCalculator;
using ParkingChargeCalculator.Models;

namespace ParkingChargeCalculatorTests
{
    [TestClass]
    public class LongStayChargeCalculatorTests
    {
        [TestMethod]
        public void ReturnsChargeForDuration()
        {
            // Arrange
            double expectedChargeToPay = 22.50;

            var startTime = new DateTime(2017, 09, 07, 7, 50, 0);
            var endTime = new DateTime(2017, 09, 09, 5, 20, 0);

            IChargeDuration chargeDuration = new LongStayChargeDuration(startTime, endTime);

            var longStayChargeCalculator = new LongStayChargeCalculator();

            // Act
            var chargeToPay = longStayChargeCalculator.CalculateCharge(chargeDuration);

            // Assert
            Assert.IsTrue(expectedChargeToPay == chargeToPay);

        }
    }
}
