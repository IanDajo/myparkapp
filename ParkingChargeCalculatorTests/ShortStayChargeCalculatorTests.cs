using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParkingChargeCalculator;
using ParkingChargeCalculator.Models;

namespace ParkingChargeCalculatorTests
{
    [TestClass]
    public class ShortStayChargeCalculatorTests
    {
        [TestMethod]
        public void ReturnsChargeForDuration()
        {
            // Arrange
            double expectedChargeToPay = 12.28;

            var startTime = new DateTime(2017, 09, 07, 16, 50, 0);
            var endTime = new DateTime(2017, 09, 09, 19, 15, 0);

            IChargeDuration chargeDuration = new ShortStayChargeDuration(startTime, endTime);

            var shortStayChargeCalculator = new ShortStayChargeCalculator();

            // Act
            var chargeToPay = shortStayChargeCalculator.CalculateCharge(chargeDuration);

            // Assert
            Assert.IsTrue(expectedChargeToPay == chargeToPay);
        }

        [TestMethod]
        public void ReturnsNoChargeForExcludedDuration()
        {
            // Arrange
            double expectedChargeToPay = 0;

            var startTime = new DateTime(2017, 09, 07, 20, 50, 0);
            var endTime = new DateTime(2017, 09, 08, 5, 25, 0);

            IChargeDuration chargeDuration = new ShortStayChargeDuration(startTime, endTime);

            var shortStayChargeCalculator = new ShortStayChargeCalculator();

            // Act
            var chargeToPay = shortStayChargeCalculator.CalculateCharge(chargeDuration);

            // Assert
            Assert.IsTrue(expectedChargeToPay == chargeToPay);
        }
    }
}
